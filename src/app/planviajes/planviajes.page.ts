import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlanificacionService } from '../services/planificacion.service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';

@Component({
  selector: 'app-planviajes',
  templateUrl: './planviajes.page.html',
  styleUrls: ['./planviajes.page.scss'],
})
export class PlanviajesPage implements OnInit {

  idusuario= null;
  idplanificacion= null;
  viajeescogido=null;
  public fechainicio= '';
  public primerfechainicial = '';
  public fechafin = '';
  public primerfechafin = '';
  constructor(private ActivatedRoute: ActivatedRoute, private planificacionService: PlanificacionService, private http: HttpClient) { }

  ngOnInit() {
    this.cargar();
  }


  cargar()
  {
    this.idusuario = this.ActivatedRoute.snapshot.paramMap.get('idusuario');
    this.idplanificacion = this.ActivatedRoute.snapshot.paramMap.get('idplanificacion');

    this.planificacionService.GetSinglePlanificacion(this.idplanificacion).then(result => result.json()).then(json => { 
      this.fechainicio =json[0]['fecha_inicio'];
      this.primerfechainicial = json[0]['fecha_inicio'];
      this.fechafin = json[0]['fecha_fin'];
      this.primerfechafin = json[0]['fecha_fin'];
    })

    this.planificacionService.GetPlanViaje(this.idplanificacion).then(result => result.json()).then(json => { 
      this.planificacionService.planviajes = json;
    })

    this.planificacionService.GetPlanViajeViajes(this.idusuario).then(result => result.json()).then(json => { 
      this.planificacionService.listaviajes = json;
    })

  }

  sendDeleteRequestViaje(inputplanificacion,inputviaje)
  {
    this.planificacionService.DeletePlanViaje(inputplanificacion,inputviaje).subscribe((response) => {
      console.log("DELETE RESULT",response);
      this.cargar();
    });
  }

  sendPostRequest()
  {
    if(this.viajeescogido != null)
    {
      this.planificacionService.PostPlanViaje(this.viajeescogido,this.idplanificacion,null,null).subscribe((response) => {
        console.log("POST RESULT",response);
        this.cargar();
      });
    }
     
  }

  InicioChange()
  {
    if(this.fechainicio != this.primerfechainicial)
    {
      console.log("fecha",this.fechainicio.substring(0,10));
        this.planificacionService.ActualizarFechaInicio(this.idusuario,this.fechainicio.substring(0,10),this.idplanificacion).subscribe((response) => {
        this.primerfechainicial = this.fechainicio;
        this.cargar();
      });
    }
    
  }

  FinChange()
  {
    if(this.fechafin != this.primerfechafin)
    {
      console.log("fecha",this.fechafin.substring(0,10));
        this.planificacionService.ActualizarFechaFin(this.idusuario,this.fechafin.substring(0,10),this.idplanificacion).subscribe((response) => {
        this.primerfechafin = this.fechafin;
        this.cargar();
      });
    }
  }
  

}
