import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from "@angular/platform-browser";
import { DebugElement } from "@angular/core";
import { PortalViajePage } from './portal-viaje.page';
import { ViajeportalService } from "src/app/services/viajeportal.service";
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { Data } from '@angular/router';
import { AngularDelegate } from '@ionic/angular';
import { IonicStorageModule } from '@ionic/storage';

describe('PortalViajePage', () => {
  let component: PortalViajePage;
  let fixture: ComponentFixture<PortalViajePage>;
  let de: DebugElement;
  let el: HTMLElement;
  let viajeportalService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PortalViajePage],
      providers: [ViajeportalService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [HttpClientTestingModule, IonicStorageModule.forRoot()]
    })
      .compileComponents();
      
      
  }));

  beforeEach(() => {

    fixture = TestBed.createComponent(PortalViajePage);
    component = fixture.componentInstance;
    fixture.detectChanges();

    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    viajeportalService = new ViajeportalService(httpClient);

  });

  afterEach(() => {
    fixture.destroy();
    component = null;
    de = null;
    el = null;
  });

  it("Given que tengo el portal definido \n When ingreso al portal \n Then la pagina es creada", () => {
    expect(fixture).toBeTruthy();
    expect(component).toBeTruthy();
  });
  
  it("Given que tengo uno o mas viajes creados \n When abro el portal \n Then el nombre de mis viajes deben mostrarse",() => {
    let esperado: boolean = true;
    let actual: boolean = true;
      const testData: Data = {Descripcion: "No se han encontrado datos", codResultado: -1 };

      viajeportalService.FetchViajes(viajeportalService.id).then(result => result.json()).then(json => {
        actual = false;
        if(JSON.stringify(json).toLowerCase() === JSON.stringify(testData).toLowerCase())
        {
          actual = true;
        }
        else
        {
          let primerviaje = json[0];
          fixture.detectChanges();
          de = fixture.debugElement.query(By.css("ion-list ion-item"));
          el = de.nativeElement;
          expect(el.textContent).toContain(primerviaje.nombre);
          actual = true;
        }
      })
      expect(actual).toBe(esperado);
  });
});