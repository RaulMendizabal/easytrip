import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed, async } from '@angular/core/testing'
import { LoginPage } from './login';
import { ServicesTest } from 'src/app/providers/servicesTest';
import { FormsModule } from '@angular/forms';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from '@angular/router';
let servicios: ServicesTest;

describe('LoginPage', () => {
  jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [FormsModule, HttpClientTestingModule, IonicStorageModule.forRoot()],
      providers: [{ provide: Router }, ServicesTest]
    }).compileComponents();

    servicios = new ServicesTest();
  }));

  it('Debe crear la pagina de login', () => {
    const fixture = TestBed.createComponent(LoginPage);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it("Debe retornar un login valido", function (done) {
    servicios.getLogin("alexanderpavelv32@gmail.com", "12345").then(response => response.json())
      .then(json => {
        if(json['codResultado'] != undefined){
          expect(json['Descripcion']).not.toBeTruthy;
          expect(json['codResultado']).toBe(-1);
        }else{
          expect(json['id_usuario']).toBe(1);
          expect(json['nombre']).toBe("pavel");
          expect(json['apellido']).toBe("vasquez");
          expect(json['email']).toBe("alexanderpavelv32@gmail.com");
          expect(json['password']).toBe("12345");
          expect(json['edad']).toBe(23);
          expect(json['foto_perfil']).toBeNull;
        }
        done();
      })
  });
});
