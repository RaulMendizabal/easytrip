import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed, async } from '@angular/core/testing'
import { PerfilUsuarioPage } from './perfil-usuario';
import { ServicesTest } from 'src/app/providers/servicesTest';
import { FormsModule } from '@angular/forms';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from '@angular/router';
let servicios: ServicesTest;
let app: any;
describe('PerfilUsuarioPage', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PerfilUsuarioPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [FormsModule, HttpClientTestingModule, IonicStorageModule.forRoot()],
      providers: [{ provide: Router }, ServicesTest]
    }).compileComponents();

    servicios = new ServicesTest();
  }));

  it('Debe crear la pagina de Perfil Usuario', () => {
    const fixture = TestBed.createComponent(PerfilUsuarioPage);
    app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it("Datos de usuario obtenidos", function (done) {
    servicios.userData(1).then(response => response.json()).then(json=>{
      console.log("entra");
      expect(json['codResultado']).toBeUndefined();
      expect(json['id_usuario']).toBe(1);
      expect(json['nombre']).toBe("pavel");
      expect(json['apellido']).toBe("vasquez");
      expect(json['email']).toBe("alexanderpavelv32@gmail.com");
      expect(json['foto_perfil']).not.toBeUndefined();
      expect(json['password']).toBe("12345");
      expect(json['edad']).toBe(23);
      done();
    });
  });
});
