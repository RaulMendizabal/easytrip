import { AfterViewInit, Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { UserData } from '../../providers/user-data';
import { Services } from '../../providers/services';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'page-perfilusuario',
  templateUrl: 'perfil-usuario.html',
  styleUrls: ['./perfil-usuario.scss'],
})
export class PerfilUsuarioPage implements AfterViewInit {
  username: string;
  descripcion: string;
  load: any;
  userId: number;
  name: string;
  lastname: string;
  foto: any;
  constructor(
    public alertCtrl: AlertController,
    public router: Router,
    public userData: UserData,
    public services: Services,
    public loadingCtrl: LoadingController,
  ) {}

  async ngAfterViewInit() {
    this.load = await this.loadingCtrl.create({
      spinner: 'lines',
      showBackdrop: true,
      message: "Espere por favor...",
      translucent: true,
      cssClass: 'ion-loading',
      duration: 0
    });
    await this.load.present();
    this.getUsername(()=>{
      this.load.dismiss(this.loadingCtrl);
    });
  }

  updatePicture() {
    console.log('click updatePicture');
  }

  async changeUsername() {
    const alert = await this.alertCtrl.create({
      header: 'Change Username',
      buttons: [
        'Cancel',
        {
          text: 'Ok',
          handler: (data: any) => {
            this.userData.setUsername(data.username);
            //this.getUsername();
          }
        }
      ],
      inputs: [
        {
          type: 'text',
          name: 'username',
          value: this.username,
          placeholder: 'username'
        }
      ]
    });
    await alert.present();
  }

  getUsername(callback) {
    this.userData.getUserId().then((userId) => {
      this.userId = userId;
      this.services.userData(1).subscribe(json=>{
        this.username = json['nombre'];
        this.foto = json['foto_perfil'];
        this.lastname = json['apellido'];
        this.descripcion = json['descripcion'];
        callback();
      });
    });
  }

  changePassword() {
    console.log('Click cambiar pw');
  }

  logout() {
    this.userData.logout();
    this.router.navigateByUrl('/login');
  }

  support() {
    this.router.navigateByUrl('/support');
  }

  Agregar(){
    console.log("Evento agregar");
  }

  Modificar(){
    console.log("Evento modificar");
  }
}
