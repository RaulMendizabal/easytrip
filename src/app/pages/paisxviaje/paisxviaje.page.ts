import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ViajeService } from 'src/app/services/viaje.service';
import { HttpClient } from '@angular/common/http';
//import { Presupuesto}  from "src/app/Presupuestos/presupuesto.spec"
import { Gestionar } from 'src/app/Presupuestos/presupuesto';

@Component({
  selector: 'app-paisxviaje',
  templateUrl: './paisxviaje.page.html',
  styleUrls: ['./paisxviaje.page.scss'],
})
export class PaisxviajePage implements OnInit {

  
  results=null;
  idviaje = null;
  idpais = null;
  paises = null;
  paisescogido = null;
  idusuario = null;
  presupuesto = null;
  tiempo =null;

  constructor(private activatedRoute: ActivatedRoute, private viajeService: ViajeService, private http: HttpClient) { }

  ngOnInit() {

    this.cargar();

  }

  
  sendPostRequest()
  {
    if(this.paisescogido != null)
    {
      this.http.post('https://easytrip-253600.appspot.com/viaje/pais', {
        id_viaje: this.idviaje,
        id_pais: this.paisescogido
      }).subscribe((response) => {
        console.log(response);
        this.cargar();
      })
    }
  } 

  SendPresupuesto(){
    this.idpais

    /*
    "id_viaje": id_viaje,
    "id_pais": id_pais, 
    "tiempo":tiempo,
    "presupuesto":cantidad$
    */
   console.log(this.idviaje);
   console.log(this.paisescogido);
   console.log(this.presupuesto);
   console.log(this.tiempo);

   Gestionar(this.idviaje,this.paisescogido,-1,this.presupuesto,this.tiempo);

  }

  sendDeleteRequestPais(paisagarrado){
    this.idpais=paisagarrado
    this.idusuario 
    this.idviaje

    if(confirm("Esta Seguro de Eliminar")){
      return this.http.delete(`https://easytrip-253600.appspot.com/${this.idusuario}/viaje/${this.idviaje}/pais/${this.idpais}`)
      .toPromise().then(()=>{
        this.cargar();
      })
     // .subscribe((response) =>
      //{console.log(response);
      //})
    }

    
    this.cargar();
  }


  cargar()
  {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    this.idusuario = id;
    let viajeid = this.activatedRoute.snapshot.paramMap.get('idviaje');

    this.idviaje = viajeid;

    this.viajeService.getPaisxViaje(id,this.idviaje).subscribe(result => {
      console.log('details',result);
      this.results = result;
    })

    this.viajeService.getPaises().subscribe(paisresult => {
      console.log('details',paisresult);
      this.paises = paisresult;
    })


  }
  

}
