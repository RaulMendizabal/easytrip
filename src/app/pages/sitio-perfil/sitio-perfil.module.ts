import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SitioPerfilPage } from './sitio-perfil';
import { SitioPerfilPageRoutingModule } from './sitio-perfil-routing.module';
import { IonicModule } from '@ionic/angular';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    SitioPerfilPageRoutingModule
  ],
  declarations: [
    SitioPerfilPage,
  ]
})
export class SitioPerfilModule { }
