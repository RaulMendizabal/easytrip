import { Component } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { ConferenceData } from '../../providers/conference-data';
import { ActivatedRoute } from '@angular/router';
import { UserData } from '../../providers/user-data';
import { Services } from '../../providers/services';

@Component({
  selector: 'page-sitio-perfil',
  styleUrls: ['./sitio-perfil.scss'],
  templateUrl: 'sitio-perfil.html'
})
export class SitioPerfilPage {
  session: any;
  sitio: any[];
  comentarios: any[];
  isFavorite = false;
  defaultHref = '';
  load: any;
  sitioId: number;
  comentario: any;
  selectValue: number;
  idUsuario: number;
  constructor(
    private dataProvider: ConferenceData,
    private userProvider: UserData,
    private route: ActivatedRoute,
    public loadingCtrl: LoadingController,
    public services: Services,
    public toastController: ToastController
  ) {
    this.selectValue = 5;
    userProvider.getUserId().then(result => {
      this.idUsuario = result;
    });
  }

  toggleFavorite() {
    if (this.userProvider.hasFavorite(this.sitioId)) {
      this.userProvider.removeFavorite(this.sitioId);
      this.isFavorite = false;
      this.services.removefav({ "id_sitio": this.sitioId, "id_usuario": this.idUsuario });
    } else {
      this.userProvider.addFavorite(this.sitioId);
      this.isFavorite = true;
      this.services.addfav({ "id_sitio": this.sitioId, "id_usuario": this.idUsuario }).subscribe();
    }
  }
  async ionViewWillEnter() {
    this.load = await this.loadingCtrl.create({
      spinner: 'lines',
      showBackdrop: true,
      message: "Espere por favor...",
      translucent: true,
      cssClass: 'ion-loading',
      duration: 0
    });
    await this.load.present();
    this.cargarperfil(() => { this.load.dismiss(this.loadingCtrl); });
  }

  async cargarperfil(callback = () => { }) {
    this.sitioId = Number(this.route.snapshot.paramMap.get('sitioId'));
    if (this.userProvider._favorites == undefined) {
      this.userProvider.getUserId().then(m => {
        this.services.getFavsUser(m).subscribe((ListaFavoritos) => {
          if (ListaFavoritos['favoritos']['codResultado'] == undefined) {
            ListaFavoritos['favoritos'].map(item => this.userProvider._favorites.push(item['id_sitio']));
            this.services.getProfileData(this.sitioId).subscribe(data => {
              this.sitio = data[0];
              this.isFavorite = this.userProvider.hasFavorite(this.sitioId);
              this.services.getCommentary(this.sitioId).subscribe(comments => {
                this.comentarios = comments["comentarios"];
                callback();
              });
            });
          }else{
            this.services.getProfileData(this.sitioId).subscribe(data => {
              this.sitio = data[0];
              this.isFavorite = this.userProvider.hasFavorite(this.sitioId);
              this.services.getCommentary(this.sitioId).subscribe(comments => {
                this.comentarios = comments["comentarios"];
                callback();
              });
            });
          }
        });
      });
    } else {
      this.services.getProfileData(this.sitioId).subscribe(data => {
        this.sitio = data[0];
        this.isFavorite = this.userProvider.hasFavorite(this.sitioId);
        this.services.getCommentary(this.sitioId).subscribe(comments => {
          this.comentarios = comments["comentarios"];
          callback();
        });
      });
    }
  }

  ionViewDidEnter() {
    this.defaultHref = `/app/tabs/sitios`;
  }

  comentar() {
    this.services.addComment({
      "id_usuario": this.idUsuario,
      "id_sitio": this.sitioId,
      "comentario": this.comentario,
      "puntuacion": this.selectValue
    }).subscribe(data => {
      this.presentToast(data);
    });
  }

  getSelectValue(selectedValue: any) {
    this.selectValue = selectedValue.detail.value;
  }

  doRefresh(event) {
    this.cargarperfil(() => {
      event.target.complete();
    });
  }

  async presentToast(data) {
    const toast = await this.toastController.create({
      message: data['codResultado'] == 0 ? "Comentario agregado correctamente." : "Ha habido un error al agregar el comentario.",
      duration: 2000,
      color: data['codResultado'] == 0 ? "success" : "danger",
    });
    toast.present();
  }
}
