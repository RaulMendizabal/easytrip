import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SitiosPage } from './sitios';
import { FiltroSitioPage } from '../sitio-filtro/sitio-filtro';
import { SitiosPageRoutingModule } from './sitios-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SitiosPageRoutingModule
  ],
  declarations: [
    SitiosPage,
    FiltroSitioPage
  ],
  entryComponents: [
    FiltroSitioPage
  ]
})
export class SitiosModule { }
