import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PlanificacionPage } from './planificacion.page';
import { By } from "@angular/platform-browser";
import { PlanificacionService } from '../services/planificacion.service';
import { HttpClient } from '@angular/common/http';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { IonicStorageModule } from '@ionic/storage';
import { Data } from '@angular/router';


describe('Pagina Planificacion', () => {
  let component: PlanificacionPage;
  let fixture: ComponentFixture<PlanificacionPage>;
  let de: DebugElement;
  let el: HTMLElement;
  let planificacionService
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlanificacionPage],
      providers: [PlanificacionService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [HttpClientTestingModule, IonicStorageModule.forRoot()]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanificacionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    planificacionService = new PlanificacionService(httpClient);
  });

  afterEach(() => {
    fixture.destroy();
    component = null;
    de = null;
    el = null;
  });

  it("Pagina creada", () => {
    expect(fixture).toBeTruthy();
    expect(component).toBeTruthy();
  });

  
  it("Mostrar planificaciones en HTML",() => {
    let esperado: boolean = true;
    let actual: boolean = true;
      const testData: Data = {Descripcion: "No se han encontrado datos", codResultado: -1 };

      planificacionService.GetPlanificacion(planificacionService.id).then(result => result.json()).then(json => {
        actual = false;
        if(JSON.stringify(json).toLowerCase() === JSON.stringify(testData).toLowerCase())
        {
          actual = true;
        }
        else
        {
          let primerplanificacion = json[0];
          fixture.detectChanges();
          de = fixture.debugElement.query(By.css("ion-list ion-item"));
          el = de.nativeElement;
          expect(el.textContent).toContain(primerplanificacion.nombre);
          actual = true;
        }
      })
      expect(actual).toBe(esperado);
  });
});