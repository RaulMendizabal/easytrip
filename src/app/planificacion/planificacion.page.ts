import { Component, OnInit } from '@angular/core';
import { PlanificacionService } from '../services/planificacion.service';
import { HttpClient } from '@angular/common/http';
import { UserData } from '../providers/user-data';

@Component({
  selector: 'app-planificacion',
  templateUrl: './planificacion.page.html',
  styleUrls: ['./planificacion.page.scss'],
})
export class PlanificacionPage implements OnInit {
  idglob: number;
  nombreplanificacion = '';
  inputnombre = '';
  constructor(public planificacionService: PlanificacionService, private http: HttpClient, public user: UserData) { }

  async ngOnInit() {
    this.traerInformacion();
  }

  async traerInformacion(callback = () => { }) {
    this.user.getUserId().then(id => {
      this.idglob = id;    
      this.planificacionService = new PlanificacionService(this.http);
      this.planificacionService.id = this.idglob;

      this.planificacionService.GetPlanificacion(id).then(result => result.json()).then(json => { 
        this.planificacionService.planificaciones = json;
      })

      callback(); 
    });
  }

  sendPostRequest()
  {

    this.planificacionService.PostPlanificacion(this.idglob,this.nombreplanificacion).subscribe((response) => {
      console.log("POST RESULT",response);
      this.cargar();
    });
    
  }

  sendDeleteRequest(inputid)
  {
    this.planificacionService.DeletePlanificacion(inputid).subscribe((response) => {
      console.log("DELETE RESULT",response);
      this.cargar();
    });
    
  }

  sendPutRequest(inputid, inputplanificacion)
  {
    this.planificacionService.ActualizarPlanificacion(inputid, this.inputnombre, inputplanificacion).subscribe((response) => {
      console.log("PUT RESULT",response);
      this.cargar();
    });
    
  }


  cargar()
  {
    this.traerInformacion();
  }

}
