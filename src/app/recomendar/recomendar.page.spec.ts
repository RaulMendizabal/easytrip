import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecomendarPage } from './recomendar.page';

import { By } from "@angular/platform-browser";
import { SitioService } from '../services/sitio.service';
import { HttpClient } from '@angular/common/http';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { IonicStorageModule } from '@ionic/storage';
import { Data } from '@angular/router';

describe('RecomendarPage', () => {
  let component: RecomendarPage;
  let fixture: ComponentFixture<RecomendarPage>;
  let de: DebugElement;
  let el: HTMLElement;
  let planificacionService
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RecomendarPage],
      providers: [SitioService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [HttpClientTestingModule, IonicStorageModule.forRoot()]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecomendarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    planificacionService = new SitioService(httpClient);
  });

  afterEach(() => {
    fixture.destroy();
    component = null;
    de = null;
    el = null;
  });

  it('GIVEN tengo codigo para mi pagina WHEN consulto recomendar sitio THEN crea pagina para el usuario', () => {
    expect(fixture).toBeTruthy();
    expect(component).toBeTruthy();
  });

  
});
