import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PortalViajexpaisPage } from './portal-viajexpais.page';

const routes: Routes = [
  {
    path: '',
    component: PortalViajexpaisPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PortalViajexpaisPage]
})
export class PortalViajexpaisPageModule {}
