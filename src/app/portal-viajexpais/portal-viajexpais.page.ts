import { Component, OnInit } from '@angular/core';
import { PortalViajexpaisService} from "../services/portal-viajexpais.service"

import { HttpClient } from '@angular/common/http';
//import { UserData } from '../../providers/user-data';
import {UserData} from "../providers/user-data"



@Component({
  selector: 'app-portal-viajexpais',
  templateUrl: './portal-viajexpais.page.html',
  styleUrls: ['./portal-viajexpais.page.scss'],
})
export class PortalViajexpaisPage implements OnInit {

  idglob: number;
  nombreviaje = '';
  inputnombre = '';
  constructor( public portalViajexpaisService: PortalViajexpaisService , private http: HttpClient, 
    
    public user: UserData
    ) { }


  async ngOnInit() {
    this.traerInformacion();
  }

  async traerInformacion(callback = () => { }) {
    this.user.getUserId().then(id => {
      this.idglob = id;    
      this.portalViajexpaisService = new PortalViajexpaisService(this.http);
      this.portalViajexpaisService.id = this.idglob;

      this.portalViajexpaisService.FetchViajes(id).then(result => result.json()).then(json => { 
        this.portalViajexpaisService.viajes = json;
      })

      callback(); 
    });
  }


}
