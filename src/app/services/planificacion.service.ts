import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PlanificacionService {
 public planificaciones: any[];
 public planviajes: any[];
 public listaviajes: any[];
 public id;
  constructor(private http: HttpClient) {

   }

  GetPlanificacion(id)
  {
    return fetch(`https://easytrip-253600.appspot.com/planificacion/porusuario/${id}`);
  }

  PostPlanificacion(inputid, inputnombre)
  {
    return this.http.post('https://easytrip-253600.appspot.com/planificacion/nuevaplanificacion', {
      nombre: inputnombre,
      id_usuario: inputid
    });

  }

  DeletePlanificacion(inputid)
  {
    return this.http.delete(`https://easytrip-253600.appspot.com/planificacion/borrar/${inputid}`);
  }

  ActualizarPlanificacion(inputid,inputnombre,inputplanificacion)
  {
    return this.http.put(`https://easytrip-253600.appspot.com/planificacion/edit/${inputplanificacion}`, {
      nombre: inputnombre,
      id_usuario: inputid
    });
  }
  PostPlanViaje(inputviaje, inputplanificacion, tiempo , presupuesto)
  {
    return this.http.post('https://easytrip-253600.appspot.com/planviaje/nuevo', {
      id_planificacion: inputplanificacion,
      id_viaje: inputviaje,
      tiempo: tiempo,
      presupuesto: presupuesto
    });
  }

  GetPlanViaje(id)
  {
    return fetch(`https://easytrip-253600.appspot.com/planviaje/getplanviaje/${id}`);
  }

  GetPlanViajeViajes(id)
  {
    return fetch(`https://easytrip-253600.appspot.com/planviaje/getviajes/${id}`);
  }

  GetSinglePlanificacion(id)
  {
    return fetch(`https://easytrip-253600.appspot.com/planificacion/una/${id}`);
  }

  DeletePlanViaje(inputplanificacion, inputviaje)
  {
    return this.http.delete(`https://easytrip-253600.appspot.com/planviaje/borrar/${inputplanificacion}/${inputviaje}`);
  }

  ActualizarFechaInicio(inputusuario,fecha,inputplanificacion)
  {
    return this.http.put(`https://easytrip-253600.appspot.com/planificacion/edit/fechainicio/${inputplanificacion}`, {
      fecha_inicio: fecha,
      id_usuario: inputusuario
    });
  }

  ActualizarFechaFin(inputusuario,fecha,inputplanificacion)
  {
    return this.http.put(`https://easytrip-253600.appspot.com/planificacion/edit/fechafin/${inputplanificacion}`, {
      fecha_fin: fecha,
      id_usuario: inputusuario
    });
  }



}
