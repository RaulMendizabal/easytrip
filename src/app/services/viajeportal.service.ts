import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http'; 
@Injectable({
  providedIn: 'root'
})
export class ViajeportalService {

  public viajes: any[];
  public id;


  constructor(private http: HttpClient) {
  

    /*this.FetchViajes(this.id).then(result => result.json()).then(json => { 
      console.log('details',json);
      this.viajes = json;
    })*/
      

  }

  FetchViajes(id)
  {
    return fetch(`https://easytrip-253600.appspot.com/viajes/${id}`);
  }

  PostViajes(inputid, inputnombre)
  {
    return this.http.post('https://easytrip-253600.appspot.com/viaje', {
      nombre: inputnombre,
      id_usuario: inputid
    });

  }

  DeleteViajes(inputid)
  {
    return this.http.delete(`https://easytrip-253600.appspot.com/viaje/${inputid}`);
  }

  ActualizarViajes(inputid,inputnombre,inputviaje)
  {
    return this.http.put(`https://easytrip-253600.appspot.com/viaje/${inputviaje}`, {
      id_viaje: inputviaje,
      nombre: inputnombre,
      id_usuario: inputid
    });
  }
  



}

