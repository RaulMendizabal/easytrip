import { Gestionar } from './presupuesto';


//Prueba para gestionar presupuesto y tiempo en viajes a paises y sitios
describe('Presupuesto, tiempo para Viajes por Pais y Sitio', () =>{
    it('Creacion de Metodo', ()=>{
        expect(Gestionar(6,1,-1,0,0)).toBeDefined();
    });

    it('Prueba de Api para viajes por pais',function (done){
        Gestionar(6,2,-1,150,30).then(response => response.json())
        .then(json => {
          if(json['codResultado'] != undefined){
            expect(json['Salida']).toBeFalsy();

          }else{
            expect(json['Salida']).toBeTruthy();
          }
          done();
        })
    });
    
    it('Prueba de Api para viaje por sitio',function (done){
        Gestionar(6,-1,1,150,30).then(response => response.json())
        .then(json => {
          if(json['codResultado'] != undefined){
            expect(json['Salida']).toBeFalsy();
          }else{
            expect(json['Salida']).toBeTruthy();
          }
          done();
        })
    });
});